#! /usr/bin/env python3.7
from . import bsdpkg
from . import freebsdports
import time
import sys
from .minorupdates import twocoldisp


def liststealthupdates(ports):
    """Print stealth updates on the screen and return package names"""
    updates=[]
    disp=[]
    ourversions=bsdpkg.loadoriginsfromfs()
    for p in ourversions.keys():
        if p not in ports or not ports[p]:
            continue    # local package
        port = None
        for po in ports[p]:
            if po.name == ourversions[p][0]:
                port = po
        if port is None:
            continue    # different versions
        # extract port version date
        t=time.localtime(port.lastmod)
        ptime=time.mktime((t[0],t[1],t[2],0,0,0,0,0,0))
        # extract out version date
        t=time.strptime(ourversions[p][1],"%Y-%m-%d")
        otime=time.mktime((t[0],t[1],t[2],0,0,0,0,0,0))
        if otime>=ptime:
            continue   # up-to-date
        delta=int((ptime-otime)//(60*60*24))
        if delta>0:
            disp.append(port.name+" %d days" % delta)
            updates.append(port.name)

    twocoldisp(disp)
    return updates


def main():
    if len(sys.argv)>1:
        print(sys.argv[0],"takes currently no arguments")
        sys.exit(1)
    ports=freebsdports.loadState()
    upd=liststealthupdates(ports)
    if len(upd)>0:
        print("Found",len(upd),"updated ports with same version number.")


if __name__ == '__main__':
    main()
