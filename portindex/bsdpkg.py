#! /usr/bin/env python3.7
import re
import time
import sqlite3
from .freebsdports import PORTS
from typing import Tuple, Dict, List, Optional

ParsedPackage = Tuple[str,str,Optional[int],Optional[int]]

LOCALPKGDB="/var/db/pkg/local.sqlite"
"""FreeBSD pkg database in SQLite format"""

def loadfromfs(db:str=LOCALPKGDB) -> List[ParsedPackage]:
    """Vraci parsovany seznam nainstalovanych baliku vcetne verzi z pkgng database."""
    rc=[]
    conn = sqlite3.connect(db)
    for row in conn.execute("SELECT name,version FROM packages"):
        fn = row[0]+"-"+row[1]
        rc.append(parsepkgname(fn))
    conn.close()
    return rc

def loadoriginsfromfs(db:str=LOCALPKGDB) -> Dict[str,Tuple[str,str]]:
    """Vraci hashtabulku originu:(jmeno baliku vcetne verze, datum instalace) z filesystemu"""
    result={}
    conn = sqlite3.connect(db)
    for row in conn.execute("SELECT name,version,origin,time FROM packages"):
        fn=row[0]+'-'+row[1]
        org=PORTS+'/'+row[2]
        t=time.localtime(row[3])
        result[org]=(fn,"%4d-%02d-%02d" % (t[0],t[1],t[2]))
    return result

# parse directory names like gconf-editor-2.4.0_5,1
def parsepkgname(name: str) -> Optional[ParsedPackage]:
    """Rozdeli jmeno baliku na tupple (jmeno,verze,release,epocha)."""
    PKGSTR=r"(.+)-(([+\.0-9a-zA-Z]+)+)(_\d+)?(,\d+)?"
    prog=re.compile(PKGSTR)
    res=prog.search(name)
    if not res:
        print("can not parse package name:",name)
        return None
    n=res.group(1)
    v=res.group(2)
    if res.group(4):
        r=res.group(4)[1:]
    else:
        r=None
    if res.group(5):
        e=res.group(5)[1:]
    else:
        e=None
    return n,v,r,e

def makeversion(parsed:ParsedPackage) -> str:
    """Vytvori verzi baliku (beze jmena) z rozdelenych casti."""
    version=parsed[1]
    if parsed[2]:
        version+='_'+str(parsed[2])
    if parsed[3]:
        version+=','+str(parsed[3])
    return version


if __name__ == "__main__":
    print(loadfromfs())
    print(loadoriginsfromfs())
