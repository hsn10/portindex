import os
import sys
import pickle
import stat
import re
import time
from . import config
from itertools import chain
from attr import attrs, attrib
from typing import Tuple, Set, Optional, Dict, List

# FreeBSD ports database system

FREEZE="portindex.pck"   # this should point to /var/db/something
PORTS="/usr/ports"
CONFIGS="/var/db/ports"
PORTLEN=len(PORTS)
MAKEFILE="Makefile"

# Tato tabulka se rucne synchronizuje s .include prikazy
# v souboru /usr/ports/Mk/bsd.port.mk
# pro snadnejsi rucni udrzbu poradi zaznamu v teto tabulce je shodne
# s poradim pouzitem v bsd.port.mk souboru
SYSINCTABLE= {
    # Direct .include in bsd.port.mk
    'PORTVERSION': 'bsd.commands.mk',
    'DESTDIR':     'bsd.destdir.mk',
    'OPTIONS_DEFINE': 'bsd.options.mk',
    'OPTIONS':     'bsd.default-versions.mk',
    'PORTNAME':    'bsd.sanity.mk',
    'USE_XORG':    'bsd.xorg.mk',
    'XORG_CAT':    'bsd.xorg.mk',
    'USE_LOCAL_MK':'bsd.local.mk',
    'USE_JAVA':    'bsd.java.mk',
    'USE_RUBY':    'bsd.ruby.mk',
    'USE_LIBRUBY': 'bsd.ruby.mk',
    'USE_OCAML':   'bsd.ocaml.mk',
    'USE_TEX':     'bsd.tex.mk',
    'USE_GECKO':   'bsd.gecko.mk',
    'USE_WX':      'bsd.wx.mk',
    'WANT_WX':     'bsd.wx.mk',
    '_GSTREAMER':  'bsd.gstreamer.mk',
    'USE_SDL':     'bsd.sdl.mk',
    '.include':    'bsd.ssp.mk',
    'USE_GCC':     'bsd.gcc.mk',
    '_OPENLDAP':   'bsd.ldap.mk',
    'MAINTAINER':  'bsd.ccache.mk',
    'LICENSE':     'bsd.licenses.mk',
    'MASTER_SITES':'bsd.sites.mk',
    '_DESC':       'bsd.options.desc.mk',
    # USES+=
    'USE_PHP':     'Uses/php.mk',
    'USE_APACHE':  'Uses/apache.mk',
    'USE_GNOME':   'Uses/gnome.mk',
    'INSTALLS_ICONS': 'Uses/gnome.mk',
    'USE_MATE':    'Uses/mate.mk',
    'USE_MYSQL':   'Uses/mysql.mk',
    # mandatory
    'bsd.port.pre.mk': 'bsd.port.mk'
}

anymasterchanged=False
"""If any master port changed we are not saving current status data on interrupt"""
baddirlist=set()
"""List of referenced directories without known port"""
MYVERSION=6
"""Serialized version of pickle file"""

@attrs(hash=True)
class portdescription(object):
    """Reprezentuje informace o portu ziskane z Makefile"""
    VERSION=0    # Placeholder, will be set on loading / saving pickle

    name   = attrib(default=None, init=False, eq=True)         # jmeno portu vcetne cele verze
    path   = attrib(default=None, init=True, eq=True)          # plna cesta k portu
    prefix = attrib(default=None, init=False, eq=False)        # root stromu portu
    comment= attrib(default="", init=False, eq=False)          # komentar
    dfile  = attrib(default="", init=False, eq=False)          # plna cesta k description file
    maintainer= attrib(default="", init=False, eq=False)       # email adresa maintainera
    categories= attrib(default="", init=False, eq=False)       # kategorie oddelene mezerou
    fdeps = attrib(default="", init=False, eq=False)           # fetch zavislosti
    edeps = attrib(default="", init=False, eq=False)      # extract zavislosti
    pdeps = attrib(default="", init=False, eq=False)      # patch zavislosti
    bdeps = attrib(default="", init=False, eq=False)      # build zavislosti
    rdeps = attrib(default="", init=False, eq=False)      # run zavislosti
    www =   attrib(default="", init=False, eq=False)      # WWW stranka
    lastmod=attrib(default=0, init=False, eq=False)       # Makefile last changed
    local=  attrib(default=0, init=False, eq=False)       # Makefile.local last changed
    inc=    attrib(default=0, init=False, eq=False)       # Makefile.inc last changed
    options=attrib(default=0, init=False, eq=False)       # /var/db/ports/{portname}/options file last changed
    sysinc= attrib(factory=set, init=False, eq=False, type=set)    # list of system includes: bsd.port.mk, ...
    masters=attrib(factory=set, init=False, eq=False, type=set)    # Master ports

    def optionsfile(self) -> str:
        """Vraci jmeno souboru pro ukladani options k tomuto portu"""
        return CONFIGS+"/"+self.categories.strip().split(" ")[0]+"_"+self.path[self.path.rfind('/')+1:]+'/options'

    def needsUpdate(self) -> bool:
        """Zjisti zda se zmenily Makefile nebo options portu a je potreba ho aktualizovat
           opetovnym nactenim informaci z jeho port Makefile.

        Vraci True pokud je potreba port aktualizat na zaklade zmenenych souboru
        """
        updateneeded = False

        if not self.path:
            raise OSError

        mk, op, ml, inc = self.getFileTimestamps()
        if mk !=self.lastmod:
            updateneeded=True
        if op!=self.options:
            updateneeded=True
        if ml!=self.local:
            updateneeded=True
        if inc!=self.inc:
            updateneeded=True
        return updateneeded

    def getFileTimestamps(self) -> Tuple[int,int,int,int]:
        """Nacte timestampy souboru Makefile, Options a Makefile.local, Makefile.inc z filesystemu.
           Nevraci exceptions pokud neexistuji ale nulu
           Vraci tupple (Makefile, Options, Makefile.local, Makefile.inc)
        """
        # Check Makefile
        makefile=self.path+"/"+MAKEFILE
        makefilelm = 0
        try:
            st=os.stat(makefile)
            makefilelm = st[stat.ST_MTIME]
        except OSError:
            pass
        # check options file
        optionslm=0
        try:
            st=os.stat(self.optionsfile())
            optionslm=st[stat.ST_MTIME]
        except OSError:
            pass
        # check Makefile.local
        mkfilelocallm=0
        try:
            st=os.stat(makefile+'.local')
            mkfilelocallm=st[stat.ST_MTIME]
        except OSError:
            pass
        # check Makefile.inc
        mkfileinclm=0
        try:
            st=os.stat(makefile+'.inc')
            mkfileinclm=st[stat.ST_MTIME]
        except OSError:
            pass

        return makefilelm, optionslm, mkfilelocallm, mkfileinclm

    @staticmethod
    def load(path:str) -> Set['portdescription']:
        """Nahraje porty z Makefile v adresari path.

           Vraci set nahranych portu
        """
        print("Loading portinfo from",path)
        res = set()
        with open(path+"/"+MAKEFILE,"r", errors="replace") as f:
            mkf=f.read()
        for desc in portdescription.make_describe(path):
            desc.sysinc=set()
            desc.masters=set()
            desc.scanIncludes(mkf)
            desc.addSystemIncludes(mkf)
            desc.addUsesIncludes(mkf)
            desc.lastmod, desc.options, desc.local, desc.inc = desc.getFileTimestamps()
            res.add(desc)

        if not res:
            print("FAILED to extract describe information from",path)
            raise OSError
        return res

    @staticmethod
    def make_describe(path:str) -> Set['portdescription']:
        """Nacte udaje z make describe. Low level funkce.

        Vraci set portdescriptions.
        """
        res = set()
        with os.popen("cd "+path+" && make describe") as p:
            lines = p.readlines()
        for ln in lines:
            spl=ln.split("|")
            if len(spl)!=13:
                print('Output of make describe has',len(spl),'fields, expected 13.')
                continue
            if path!=spl[1]:
                print("Warning: port path is",path,"but Makefile reports",spl[1])
            desc = portdescription()
            desc.name=spl[0]
            desc.path=spl[1]
            desc.prefix=spl[2]
            desc.comment=unescapeMakefile(re.sub(r'\s{2,}',' ',spl[3].strip()))
            desc.dfile=spl[4]
            desc.maintainer=unescapeMakefile(spl[5].strip())
            desc.categories=spl[6].strip()
            desc.edeps=spl[7].strip()
            desc.pdeps=spl[8].strip()
            desc.fdeps=spl[9].strip()
            desc.bdeps=spl[10].strip()
            desc.rdeps=spl[11].strip()
            desc.www=spl[12].strip()
            res.add(desc)
        return res

    def addSystemIncludes(self,makefile:str) -> None:
        """Prida dodatecne zavislosti na systemovych include souborech podle preddefinovane tabulky."""
        for s in SYSINCTABLE.keys():
            if s in makefile:
                self.sysinc.add(SYSINCTABLE[s])

    def addUsesIncludes(self, makefile:str) -> None:
        """Prida dodatecne zavislosti na Uses/*.mk"""
        usesrx=r"^\s*USES\s*\+?=\s*((\S+[ \t]*)+)$"
        usesrx=re.compile(usesrx, re.MULTILINE)
        modulerx=r"(\w+)(?::\w+)*"
        modulerx=re.compile(modulerx)
        matches=re.finditer(usesrx, makefile)
        for match in matches:
            for include in re.finditer(modulerx,match.group(1)):
                self.sysinc.add("Uses/"+include.group(1)+".mk")

    def findMakeVariable(self,var:str,makefile:str,wantempty:bool=False) -> Optional[str]:
        """Najde promennou v Makefile. Pokud je wantempty True tak
           se ji nepokousi vyextrahovat pomoci make -V kdyz ji nenajde"""
        vrx=r'^\s*'+var+r'\s*\??=\s*(\S+)\s*$'
        vrx=re.compile(vrx,re.MULTILINE)
        matches=re.findall(vrx,makefile)
        if len(matches)==1:
            return matches[0]
        elif len(matches)==0:
            if not wantempty:
                print("Var",var,"not found by scanning Makefile",self.path)
        else:
            print("Var",var,"found multiple times in Makefile",self.path)
            wantempty=False
        # fallback to make -V if needed
        if wantempty is False:
            print("Extracting variable",var,"from",self.path,"using make -V")
            with os.popen("cd "+self.path+" && make -V "+var) as p:
                ln=p.readlines()
            if len(ln)==1:
                return ln[0].strip()
            print("Extraction FAILED. Output was: ",ln)
        return None

    @staticmethod
    def H_operator(path:str) -> str:
        """Vraci :H operator. Pouziva se casto na .CURDIR.
           Operator vraci nadrazeny adresar.
        """
        try:
            return path[:path.rindex('/')]
        except ValueError:
            return ''

    def scanIncludes(self,makefile:str) -> None:
        """Prohleda makefile na .include soubory a prida je do zavislosti."""
        INCRX=r'^\s*.include\s+[<"](\S+)[>"]\s*$'
        VARRX=r'\$\{(\S+?)\}'
        inx=re.compile(INCRX,re.MULTILINE)
        includes=re.findall(inx,makefile)
        for i in includes:
            if i.startswith('bsd.'):
                self.sysinc.add(i)
                continue
            # print "Expanding:",i
            # expand variables
            while True:
                variables=re.search(VARRX,i)
                if variables is None:
                    break
                v=variables.group(1)
                value=""
                if v=='.CURDIR':
                    value=self.path
                elif v=='.CURDIR:H':
                    value = portdescription.H_operator(self.path)
                elif v=='.CURDIR:H:H':
                    value = portdescription.H_operator(portdescription.H_operator(self.path))
                elif v=='PORTSDIR':
                    value=PORTS
                elif v=='WRKDIRPREFIX':
                    value=""
                elif v=='WRKDIR':
                    value=self.path+'/work'
                elif v=='OPTIONS_FILE':
                    value=self.findMakeVariable(v,makefile,True)
                    if not value:
                        value=self.optionsfile()
                elif v=='FILESDIR':
                    value=self.findMakeVariable(v,makefile,True)
                    if not value:
                        md=self.findMakeVariable('MASTERDIR',makefile,True)
                        if md is None:
                            value=self.path+'/files'
                        else:
                            value=md+'/files'
                else:
                    value=self.findMakeVariable(v,makefile)
                i=i.replace("${"+v+"}",value)
            # resolve path
            i=os.path.join(self.path,i)
            i=os.path.normpath(i)
            # remove links to outside
            if i[:PORTLEN+1] != PORTS+'/':
                # print "**** not going outside ports tree"
                continue
            i=i[PORTLEN+1:]
            spl=i.split('/')
            if spl[0]=='Mk':
                # print "*** points to system include",spl[1]
                self.sysinc.add(spl[1])
                continue
            # Found target port
            i=PORTS+'/'+str(spl[0])+'/'+str(spl[1])
            if i == self.path:
                continue
            if spl[1] == "Makefile.inc":
                continue    # ignore category makefiles
            print("  Master port is",i)
            self.masters.add(i)

def checkForOutdatedPort(dirname:str,ports:Dict[str,Set[portdescription]],liveports:Dict[str,Optional[int]]) -> None:
    global anymasterchanged
    if dirname not in ports or not ports[dirname]:
        p=portdescription(dirname)
        ports[dirname]={p}
    else:
        p=next(iter(ports[dirname]))
    if dirname in liveports:
        return   # already scanned
    try:
        needUpdate = p.needsUpdate()
        if not needUpdate:
            # check master ports for changes
            masters=set()
            for variant in ports[dirname]:
                masters.update(variant.masters)
            for inc in masters:
                if inc in ports:
                    if inc not in liveports:
                        # we need to check/update master first
                        checkForOutdatedPort(inc,ports,liveports)
                else:
                    continue   # master is unknown, ignore this master for now
                if inc not in liveports:
                    # master port update failed, kill slave port also
                    return
                if liveports[inc] is not None:
                    needUpdate=True
                    print("Regenerating",p.name,"because master",inc,"changed.")
                    anymasterchanged=True
                    break
            if needUpdate is False:
                # check also system includes for changes
                systemincludes=set()
                for variant in ports[dirname]:
                    systemincludes.update(variant.sysinc)
                for inc in systemincludes:
                    if inc in liveports and liveports[inc] is not None:
                        needUpdate=True
                        print("Regenerating",p.name,"because",inc,"changed.")
                        break
        if needUpdate is True:
            ports[p.path] = set()
            for p in portdescription.load(dirname):
                liveports[p.path]=1
                ports[p.path].add(p)
        else:
            liveports[dirname]=None    # port is stale
    except OSError:
        pass    # port is dead

def updatePortsInfo(allports:Dict[str,Set[portdescription]],start:str=PORTS,liveports:Dict[str,Optional[int]]=None) -> None:
    global anymasterchanged
    anymasterchanged=False
    if liveports is None:
        liveports={}
    lminfo={}
    # check system includes
    for d in chain(filter(lambda x: stat.S_ISREG(os.stat(start + "/Mk/" + x)[stat.ST_MODE]), os.listdir(start+'/Mk')),
                   map(lambda x: 'Uses/'+x, os.listdir(start+'/Mk/Uses'))):
        cat=start+'/Mk/'+d
        lm=os.stat(cat)[stat.ST_MTIME]
        lminfo[d]=lm
        try:
            if next(iter(allports[d])).lastmod !=lm:
                print("System include",cat,"has changed.")
                delta=(time.time()-next(iter(allports[d])).lastmod)//3600
                print("   File changed after",int(delta),"hours from last known state.")
                if delta<config.SYSINCIGNORETIME:
                    print("   Change is ignored until it becomes at least",config.SYSINCIGNORETIME,"hours old.")
                    liveports[d]=None
                    lminfo[d]=next(iter(allports[d])).lastmod
                else:
                    liveports[d]=1
            else:
                liveports[d]=None
        except KeyError:
            print("New system include",cat,"found!")
            liveports[d]=None

    print("Checking ports tree for changes. Abort with CTRL-C.")
    try:
        categories,_junk=parseCategoryMakefile(start)
        for cat in categories:
            cat=start+'/'+cat
            subdirs,_junk=parseCategoryMakefile(cat)
            for d in subdirs:
                checkForOutdatedPort(cat+'/'+d,allports,liveports)

        # delete dead ports
        toberemoved=set()
        for d in allports.keys():
            if d not in liveports:
                toberemoved.add(d)
        for delme in toberemoved:
            del allports[delme]
            print("Removing port",delme,"from database")

        # update lastmod info for system includes
        for d in lminfo.keys():
            si = portdescription()
            si.path = d
            si.lastmod = lminfo[d]
            allports[d]={si}
        anymasterchanged=False

    except KeyboardInterrupt:
        pass

def loadState(fname:str=FREEZE) -> Dict[str,Set[portdescription]]:
    allports={}
    try:
        with open(fname,"rb") as f:
            allports=pickle.load(f)
        # check version
        if allports['__VERSION__'] != MYVERSION:
            raise AttributeError('Version mismatch')
        del allports['__VERSION__']
    except KeyboardInterrupt:
        sys.exit(1)
    except Exception as e:
        print("Loading status data failed (",e,")")
        allports={}
    portdescription.VERSION=MYVERSION
    return allports

def saveState(allports,fname:str=FREEZE) -> bool:
    try:
        if anymasterchanged is False:
            with open(fname,"wb") as f:
                print("Saving status data ...",end='',flush=True)
                allports['__VERSION__']=MYVERSION
                portdescription.VERSION=MYVERSION
                pickle.dump(allports,f,-1)
                del allports['__VERSION__']
            print(" ok!")
        else:
            print("Not saving status data because there are in inconsistent state now.")
            print("This is most likely due to program interuption during port tree scan.")
    except KeyboardInterrupt:
        os.unlink(fname)
        sys.exit(1)
    except Exception as e:
        print("Error when saving status data.",e)
        os.unlink(fname)
        return False
    return True

def _expandPortField(p:portdescription,field:str,ports:Dict[str,Set[portdescription]],expandcache:Set[str],abspathcache:Dict[str,str]) -> None:
    if len(getattr(p,field))>0:
        result={}
        for e in getattr(p,field).split(" "):
            try:
                e=abspathcache[e]
            except KeyError:
                ne=os.path.abspath(e)
                abspathcache[e]=ne
                e=ne
            try:
                result[e]=None
                if e not in expandcache:
                    for dp in ports[e]:
                        _expandPortField(dp,'rdeps',ports,expandcache,abspathcache)
                    expandcache.add(e)
                # merge his R-depends into ours
                np=next(iter(ports[e]))
                if len(np.rdeps)==0:
                    continue
                for d in np.rdeps.split(" "):
                    result[d]=None
            except KeyError:
                pass

        setattr(p,field,' '.join(result.keys()))

def expandAllDepends(ports:Dict[str,Set[portdescription]]) -> None:
    expandcache=set()
    """Origins of ports with already expanded dependencies"""
    abspathcache={}
    """Dictionary relative -> absolute path"""

    print("Expanding dependences ...",end='',flush=True)
    for s in ports.values():
        for p in s:
            if p.path not in expandcache:
                _expandPortField(p,"rdeps",ports,expandcache,abspathcache)
                expandcache.add(p.path)
            _expandPortField(p,"bdeps",ports,expandcache,abspathcache)
            _expandPortField(p,'fdeps',ports,expandcache,abspathcache)
            _expandPortField(p,'edeps',ports,expandcache,abspathcache)
            _expandPortField(p,'pdeps',ports,expandcache,abspathcache)
    print(" done")

def directoriesToNames(dirlist:str,ports:Dict[str,Set[portdescription]],ignoreunresolved:bool=False) -> str:
    spl=dirlist.split(" ")
    result=[]
    for d in spl:
        try:
            p=ports[d]
            name=next(iter(p)).name   # TODO name is randomly picked from list
            if name:
                result.append(name)
            else:
                raise KeyError(d)
        except KeyError as k:
            if len(d)==0:
                continue
            if d not in baddirlist:
                print("No port known in",d)
                baddirlist.add(d)
            if not ignoreunresolved:
                raise k
    result.sort()
    return " ".join(result)

def parseCategoryMakefile(directory:str) -> Tuple[List[str],List[str]]:
    """Returns ( [subdir, ...],[ 'comment'] )"""
    SUBDIRRX=r"^\s*SUBDIR\s*\+=\s*(\S+)"
    COMMENTRX=r'$\s*COMMENT\s*=\s*(.+?)\s*^'
    srx=re.compile(SUBDIRRX,re.MULTILINE)
    crx=re.compile(COMMENTRX,re.MULTILINE)
    try:
        with open(directory+'/'+MAKEFILE,'r') as f:
            makefile=f.read()
    except IOError:
        makefile=''
    return re.findall(srx,makefile), re.findall(crx,makefile)

def unescapeMakefile(what:str) -> str:
    """Dekoduje backslash escapy. ktere se pouzivaji v makefile."""
    return re.sub(r"\\(.)",r"\1",what)
