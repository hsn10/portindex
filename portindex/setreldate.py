#! /usr/bin/env python3.7
# Grabs archived tarball from the database

import sys
from .loadindex import findworkingdbconnection
from .query import find_package

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("Usage: python3",sys.argv[0],"<package> <version> <release-date>")
    else:
        c=findworkingdbconnection()
        pkg=find_package(c,sys.argv[1])
        if pkg is not None:
            q=c.query("SELECT version from versions WHERE packageid="+str(pkg['id'])+" AND version='"+sys.argv[2]+"'")
            res=q.dictresult()
            if q.ntuples() == 0:
                print("ERROR: This package version is not available")
                print("Hint: Use query",sys.argv[1],"to see list of package versions.")
            else:
                q=c.query("UPDATE versions SET released='%s' WHERE packageid=%d AND version='%s'" % (sys.argv[3],pkg['id'],sys.argv[2]))
        c.close()
