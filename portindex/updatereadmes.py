#! /usr/bin/env python3.7
import os
import stat
import sys
import getopt
from . import freebsdports
from html.entities import codepoint2name
from typing import Dict, Set, List

README="README.html"
READMETMPL="/Templates/README.port"
CATEGTMPL= "/Templates/README.category"
TOPTMPL= "/Templates/README.top"
CATEGORYTEMPLATE:str=""
READMETEMPLATE:str=""
TOPTEMPLATE:str=""
MAKEFILE="Makefile"
MAXSTALEDAYS=0
FORCECATEGORIES=False


def escapeHTML(plain:str) -> str:
    html=[]
    for c in plain:
        try:
            ec=codepoint2name[ord(c)]
            html.append('&'+ec+';')
        except KeyError:
            html.append(c)
    return ''.join(html)

def generateReadme(directory:str,ports:Dict[str,Set[freebsdports.portdescription]]) -> None:
    if directory not in ports or not ports[directory]:
        print("No port known in",directory,"using make readme")
        os.system("cd "+directory+" && make readme")
    else:
        p=next(iter(ports[directory]))
        s=READMETEMPLATE.replace("%%PORT%%",directory[freebsdports.PORTLEN+1:])
        s=s.replace("%%PKG%%",p.name)
        s=s.replace("%%COMMENT%%",escapeHTML(p.comment))
        s=s.replace("%%DESCR%%",p.dfile[len(directory)+1:])
        s=s.replace("%%EMAIL%%",p.maintainer)
        s=s.replace("%%TOP%%","../..")
        if len(p.www)<5:
            s=s.replace("%%WEBSITE%%",'')
        else:
            s=s.replace("%%WEBSITE%%",' and/or visit the <a href="'+p.www+'">web site</a> for futher informations')
        if len(p.bdeps)>1:
            s=s.replace("%%BUILD_DEPENDS%%",'This port requires package(s) "'+freebsdports.directoriesToNames(p.bdeps,ports,True)+'" to build.')
        else:
            s=s.replace("%%BUILD_DEPENDS%%",'')
        if len(p.rdeps)>1:
            s=s.replace("%%RUN_DEPENDS%%",'This port requires package(s) "'+freebsdports.directoriesToNames(p.rdeps,ports,True)+'" to run.')
        else:
            s=s.replace("%%RUN_DEPENDS%%",'')
        try:
            with open(directory+"/"+README,"w") as f:
                f.write(s)
        except IOError:
            print("Failed to write",directory+"/"+README)

def generateTop(dirname:str,subdirs:List[str],ports:Dict[str,Set[freebsdports.portdescription]]) -> None:
    subdir=''
    subdirs.sort()
    for d in subdirs:
        junk,comment=freebsdports.parseCategoryMakefile(dirname+'/'+d)
        if len(comment)>0:
            comment=escapeHTML(comment[0])
        else:
            comment=''
        subdir+='<a href="%s/README.html">%s</a>: %s\n' % (d,d,comment)
    s=TOPTEMPLATE.replace("%%SUBDIR%%",subdir)
    with open(dirname+"/"+README,"w") as f:
        f.write(s)

def generateCategory(directory:str,ports:Dict[str,Set[freebsdports.portdescription]]):
    subdirs,comment=freebsdports.parseCategoryMakefile(directory)
    s=CATEGORYTEMPLATE.replace("%%CATEGORY%%",directory[freebsdports.PORTLEN+1:])
    s=s.replace("%%DESCR%%","")
    if len(comment)>0:
        s=s.replace("%%COMMENT%%",escapeHTML(comment[0]))
    else:
        s=s.replace("%%COMMENT%%",'')
    # now generate subdirectory listings
    subdir=[]
    subdirs.sort()
    for d in subdirs:
        try:
            p=next(iter(ports[directory+"/"+d]))
            subdir.append('<a href="%s/README.html">%s</a>: %s\n' % (d,p.name,escapeHTML(p.comment)))
        except KeyError:
            pass
    s=s.replace("%%SUBDIR%%",''.join(subdir))
    try:
        with open(directory+"/"+README,"w") as f:
            f.write(s)
    except IOError:
        print("Failed to write",directory+"/"+README)

def checkForOutdatedCategory(dirname,ports:Dict[str,Set[freebsdports.portdescription]]):
    rebuild=False
    if MAXSTALEDAYS<0 or FORCECATEGORIES:
        rebuild=True
    else:
        try:
            rm=os.stat(dirname+'/'+README)
            mk=os.stat(dirname+'/'+MAKEFILE)
            if mk[stat.ST_MTIME]>rm[stat.ST_MTIME]:
                rebuild=True
        except OSError:
            rebuild=True
    if rebuild:
        print("Generating category",README,"in",dirname)
        generateCategory(dirname,ports)

def checkForOutdatedTop(dirname,subdirs,ports:Dict[str,Set[freebsdports.portdescription]]):
    rebuild=False
    if MAXSTALEDAYS<0:
        rebuild=True
    else:
        try:
            rm=os.stat(dirname+'/'+README)
            mk=os.stat(dirname+'/'+MAKEFILE)
            if mk[stat.ST_MTIME]>rm[stat.ST_MTIME]:
                rebuild=True
            for d in subdirs:
                mk=os.stat(dirname+'/'+d+'/'+MAKEFILE)
                if mk[stat.ST_MTIME]>rm[stat.ST_MTIME]:
                    rebuild=True
                    break
        except OSError:
            rebuild=True
    if rebuild:
        print("Generating Top",README,"in",dirname)
        generateTop(dirname,subdirs,ports)

def updateCategories(ports:Dict[str,Set[freebsdports.portdescription]]):
    subdirs,junk=freebsdports.parseCategoryMakefile(freebsdports.PORTS)
    checkForOutdatedTop(freebsdports.PORTS,subdirs,ports)
    for d in subdirs:
        checkForOutdatedCategory(freebsdports.PORTS+"/"+d,ports)

def checkForOutdatedReadme(ports:Dict[str,Set[freebsdports.portdescription]],dirname,names):
    if dirname[freebsdports.PORTLEN:].count("/") != 2:
        return
    if len(names)==1 and names[0]==README:
        print("Removing directory",dirname)
        os.unlink(os.path.join(dirname,README))
        os.rmdir(dirname)
    elif MAKEFILE not in names:
        pass
    elif README not in names:
        print("Generating missing",README,"in",dirname)
        generateReadme(dirname,ports)
    else:
        regen=False
        if MAXSTALEDAYS>=0:
            rm=os.stat(dirname+'/'+README)
            mk=os.stat(dirname+'/'+MAKEFILE)
            dif=mk[stat.ST_MTIME]-rm[stat.ST_MTIME]
            if dif>0:
                dif//=60*60*24
                if dif>=MAXSTALEDAYS:
                    regen=True
        else:
            regen=True
        if regen:
            print("Regenerating",README,"in",dirname)
            generateReadme(dirname,ports)
    del names[:]

def loadTemplates() -> None:
    global READMETEMPLATE,CATEGORYTEMPLATE,TOPTEMPLATE
    with open(freebsdports.PORTS+READMETMPL) as f:
        READMETEMPLATE=f.read()
    with open(freebsdports.PORTS+CATEGTMPL) as f:
        CATEGORYTEMPLATE=f.read()
    with open(freebsdports.PORTS+TOPTMPL) as f:
        TOPTEMPLATE=f.read()


def main() -> None:
    global MAXSTALEDAYS
    global FORCECATEGORIES
    for opt in getopt.getopt(sys.argv[1:],"d:chf?")[0]:
        if opt[0]=='-d':
            MAXSTALEDAYS=int(opt[1])
        elif opt[0]=='-f':
            MAXSTALEDAYS=-9999999
        elif opt[0]=='-c':
            FORCECATEGORIES=True
        elif opt[0]=='-h' or opt[0]=='-?':
            print("python",sys.argv[0]," [ options ]")
            print("This program (re)generates README.html files in ports tree.")
            print("Options:")
            print("  -d days  update only Readme which are at least -days- old.")
            print("  -f       force updating of all ports Readme files.")
            print("  -c       force Readme updating in all categories.")
            print("  -h       This help")
            sys.exit(0)
    ports=freebsdports.loadState()
    freebsdports.updatePortsInfo(ports)
    freebsdports.saveState(ports)
    loadTemplates()
    updateCategories(ports)
    freebsdports.expandAllDepends(ports)

    for w in os.walk(freebsdports.PORTS):
        checkForOutdatedReadme(ports, w[0], w[2])

if __name__ == "__main__":
    main()
