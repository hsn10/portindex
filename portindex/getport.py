#! /usr/bin/env python3.7
# Grabs archived tarball from the database

import sys
import os
import pg
from .loadindex import findworkingdbconnection, pkg, prepare_statements
from .query import find_package
from typing import Tuple, Optional


def gettarball(c,dbpkg:pkg, version:str) -> Optional[Tuple[bytes,int]]:
    """Vrati (binary encoded tarball,datum) pro danou verzi baliku pkg nebo None"""
    # We need to get packageid first
    q=c.query("EXECUTE S1('%s')" % dbpkg.pkgdir)
    if q.ntuples() == 0:
        return None
    res=q.dictresult()[0]
    # Find if we have an tarball archived
    q=c.query("SELECT tar,date_part('epoch',packed) AS packed FROM tarballs WHERE packageid=" + str(res['id']) + " AND version='" + version + "' ORDER BY packed DESC")
    res=q.dictresult()
    if q.ntuples() == 0:
        return None
    else:
        if q.ntuples() > 1:
            print("More than one tarball archived for",dbpkg.package, version, "using latest.")
        return res[0]['tar'],res[0]['packed'] # unescape bytea here


def main() -> None:
    if len(sys.argv) != 3:
        print("Usage: python",sys.argv[0],"<package> <version>")
    else:
        c=findworkingdbconnection()
        prepare_statements(c)
        dbpkg=find_package(c,sys.argv[1])
        if dbpkg is not None:
            realpkg = pkg()
            realpkg.updatefromdb(dbpkg, dbpkg)
            # print(realpkg, realpkg.pkgdir)
            ball=gettarball(c,realpkg,sys.argv[2])
            if ball is None:
                print("ERROR: No tarballs archived for requested version")
                print("Hint: Use query",sys.argv[1],"to see if archived tarballs are available.")
            else:
                fname=sys.argv[1]+'-'+sys.argv[2]+'-portbld.tar.gz'
                with open(fname,'wb') as f:
                    f.write(ball[0])
                stamp=ball[1]
                os.utime(fname,(stamp,stamp))


if __name__ == '__main__':
    main()
