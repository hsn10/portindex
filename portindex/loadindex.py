#! /usr/bin/env python3.7
# Loads available packages from INDEX into into database

# python load.py [-n] [ filename ...]
# command line arguments
#   -n                 do not update package description
#   filename    import this filename (Debian available file format)
#               if no filename use apt-cache dumpavail

import os
import re
import pg
import sys
from datetime import datetime
from dateutil import parser
from stat import ST_MTIME
import gzip
import getopt
from . import bsdpkg
from . import config
from . import freebsdports
from .freebsdports import portdescription
from attr import attrs, attrib
from typing import TextIO, Optional, List, Dict, Any, Set, Union

dictresult = Dict[str, Any]
USEDATE=datetime.now()
"""Datum pro released polozku v balicku"""
USEDATESTR=None

@attrs(hash=True)
class pkg(object):
    dbinternal={'id','alive','packageid','install'}

    package  = attrib(default = None, init = False)
    pkgdir   = attrib(default = None, init = False)
    section  = attrib(default = None, init = False)
    descr    = attrib(default = None, init = False)
    maintainer = attrib(default = None, init = False)
    version  = attrib(default = None, init = False)
    bdepends = attrib(default = None, init = False)
    rdepends = attrib(default = None, init = False)
    edepends = attrib(default = None, init = False)
    fdepends = attrib(default = None, init = False)
    pdepends = attrib(default = None, init = False)
    www      = attrib(default = None, init = False)
    released = attrib(default = None, init = False)
    installed =attrib(default = None, init = False)

    def updatefromdb(self,res1:dictresult,res2:dictresult) -> None:
        for fieldlist in (res1.keys(), res2.keys()):
            for field in fieldlist:
                if field is None:
                    continue
                if field in pkg.dbinternal:
                    continue
                if hasattr(self,field):
                    setattr(self,field,res1.get(field) or res2.get(field))
                else:
                    print("pkg object has no atr",field)

    def savetodb(self,c,userealdates:bool,allports:Dict[str, Set[portdescription]]=None) -> None:
        """saves package object into database"""
        # check for existence of package record
        q=c.query("EXECUTE S1(%s)" % quote(self.pkgdir))
        if q.ntuples()==0:
            # create a new database record
            query="EXECUTE I1(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (quote(self.package),quote(self.pkgdir),quote(self.section),quote(self.descr),quote(self.maintainer),quote(self.bdepends),quote(self.rdepends),quote(self.www),quote(self.edepends),quote(self.pdepends),quote(self.fdepends))
            print("Inserting NEW PACKAGE",self.pkgdir,"to database.")
            try:
                q=c.query(query)
            except pg.Error:
                print("ERROR!",query)
                return
            q=c.query("EXECUTE S1(%s)" % quote(self.pkgdir))
            res=q.dictresult()[0]
        else:
            # we have existing db record
            res=q.dictresult()[0]
            # check if main record needs to be updated
            changed=''
            if res['section']  != self.section:  changed+=' section'
            if res['descr']    != self.descr:    changed+=' descr'
            if res['package']  != self.package:  changed+=' package'
            if res['maintainer'] != self.maintainer: changed+=' maintainer'
            if res['rdepends'] != self.rdepends: changed+=' rdeps'
            if res['bdepends'] != self.bdepends: changed+=' bdeps'
            if res['edepends'] != self.edepends: changed+=' edeps'
            if res['pdepends'] != self.pdepends: changed+=' pdeps'
            if res['fdepends'] != self.fdepends: changed+=' fdeps'
            if res['www']      != self.www:      changed+=' www'
            if len(changed)>0:
                query="EXECUTE U1(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" % (quote(self.section),quote(self.descr),quote(self.package),quote(self.maintainer),quote(self.rdepends),quote(self.bdepends),quote(self.www),quote(self.pkgdir),quote(self.edepends),quote(self.pdepends),quote(self.fdepends))
                if changed.strip() != '':
                    print("Updating",self.pkgdir,'New:'+changed)
                c.query(query)

        if userealdates:
            if self.released is None:
                if allports and self.pkgdir in allports and allports[self.pkgdir]:
                    self.released=datetime.isoformat(datetime.fromtimestamp(next(iter(allports[self.pkgdir])).lastmod))
                else:
                    self.released=datetime.isoformat(datetime.fromtimestamp(os.stat(self.pkgdir+'/Makefile')[ST_MTIME]))
            #  check if any master ports are newer then us, if yes use their date
            if allports and self.pkgdir in allports and allports[self.pkgdir]:
                nasdatum=parser.parse(self.released)
                # najdeme se v portech
                nasport = None
                for moznamy in allports[self.pkgdir]:
                    if self.package == bsdpkg.parsepkgname(moznamy.name)[0]:
                        nasport = moznamy
                    if nasport is None:
                        nasport = next(iter(allports[self.pkgdir]))
                    for masterport in nasport.masters:
                        if masterport not in allports:
                            continue   # unknown master port
                        masterportdate=datetime.fromtimestamp(next(iter(allports[masterport])).lastmod)
                        if masterportdate > nasdatum:
                            nasdatum = masterportdate
                    self.released=str(nasdatum)

        # check for existence of version record
        q=c.query("EXECUTE S2(%d,%s)" % (res['id'],quote(self.version)))
        if q.ntuples()==0:
            # create a new version record
            print("Inserting new version",self.version,"of package",self.pkgdir,"to database.")
            query="EXECUTE I2(%s,%s,%d,%s)" % (quote(self.released or USEDATESTR),quote(self.installed),res['id'],quote(self.version))
            try:
                q=c.query(query)
            except pg.Error:
                print("ERROR!",query)
        else:
            # update OLD version record
            res=q.dictresult()[0]
            if userealdates:
                # update released date
                if self.released is not None and not self.released.startswith(res['released']):
                    print("Updating released date for version",self.version,"of",self.pkgdir)
                    query="UPDATE versions SET released='"+self.released+"' WHERE version='"+res['version']+"' AND packageid="+str(res['packageid'])
                    try:
                        q=c.query(query)
                    except pg.Error:
                        print(query)
            # update installed date
            if self.installed:
                if self.installed != res['installed']:
                    print("Updating installed date for version",self.version,"of",self.package)
                    query="UPDATE versions SET installed='"+self.installed+"' WHERE version='"+res['version']+"' AND packageid="+str(res['packageid'])
                    try:
                        q=c.query(query)
                    except pg.Error:
                        print(query)

    @staticmethod
    def unescape_index(what:str) -> Optional[str]:
        """Odstrani escape sekvence co se pouzivaji ve FreeBSD INDEX souboru"""
        if what is None:
            return None
        else:
            return re.sub(r"\\(['*])",r"\1",what)

    def load(self,file:TextIO) -> None:
        """loads object values from Freebsd INDEX file format"""
        entry=file.readline()
        if entry=='':
            return     # end of file
        entry=entry.strip()
        if entry=='':
            return
        spl:List[Optional[str]]=entry.split('|')
        if len(spl)!=13:
            print("Bad index entry (not 13 fields): ",entry)
            return
        for i in range(0,13):
            if spl[i]=='':
                spl[i]=None
        package,pdir,root,comment,descrfile,maint,cat,bdep,rdep,www,edep,pdep,fdep=spl
        # parse package name
        parsed=bsdpkg.parsepkgname(package)
        self.package=parsed[0]
        self.pkgdir=pdir
        self.version=bsdpkg.makeversion(parsed)
        self.descr=pkg.unescape_index(comment)
        self.maintainer=pkg.unescape_index(maint)
        self.section=cat
        self.bdepends=bdep
        self.rdepends=rdep
        self.www=www
        self.edepends=edep
        self.pdepends=pdep
        self.fdepends=fdep

def quote(stri:str) -> str:
    """Prevede Python object na pgsql parametr. Respektuje hodnotu NULL."""
    if stri is None:
        return "NULL"
    if isinstance(stri,int):
        return str(stri)
    return "E'"+pg.escape_string(stri)+"'"

def db_loadpkg(c,name:str,wantversion:Optional[str]=None) -> Union[pkg,None,List[pkg]]:
    """load multiple versions of package from database"""
    q=c.query("EXECUTE S1(%s)" % quote(name))
    if q.ntuples()==0:
        return None
    res=q.dictresult()[0]

    if wantversion:
        query="EXECUTE Q2(%d,%s)" % (res['id'],quote(wantversion))
    else:
        query="EXECUTE Q1(%d)" % res['id']
    q=c.query(query)
    if q.ntuples()==0:
        return None
    if wantversion:
        p=pkg()
        p.updatefromdb(res,q.dictresult()[0])
        return p
    res2=q.dictresult()
    grandresult=[]
    for version in res2:
        p=pkg()
        p.updatefromdb(res,version)
        grandresult.append(p)
    return grandresult

def loadfile(f:TextIO,c,usemakefiledates:bool=True,porthints:Dict[str, Set[portdescription]]=None) -> None:
    """loads all entries from FreeBSD index file to database"""
    global USEDATESTR
    USEDATESTR=str(USEDATE)
    c.query('BEGIN TRANSACTION')
    while True:
        entry=pkg()
        entry.load(f)
        if entry.package is None:
            break
        if entry.version is None:
            continue
        entry.savetodb(c,usemakefiledates,porthints)
    c.query('END')

def findworkingdbconnection() -> Any:
    """Returns database connection to database with required tables
       Exits to OS on failure
    """
    for con in config.DATABASE_CONNECTIONS:
        try:
            spl=con.split(' ')
            if len(spl)==1:
                # short format, no key value pairs
                c=pg.connect(con)
            else:
                # extract key=value pairs from connection string
                kwargs={}
                for a in spl:
                    spl2=a.split('=')
                    if len(spl2)!=2:
                        continue
                    kwargs[spl2[0].strip()]=spl2[1].strip()
                # extract schema
                schema = None
                if "schema" in kwargs:
                    schema = kwargs['schema']
                    del kwargs['schema']
                c=pg.connect(**kwargs)
                if schema is not None:
                    c.query("SET SEARCH_PATH=%s" % schema)
            c.query("SELECT 1 FROM packages LIMIT 1")
            c.query("SELECT 1 FROM versions LIMIT 1")
            c.query("SELECT 1 FROM tarballs LIMIT 1")
            c.query('SET DateStyle=ISO')
            return c
        except pg.InternalError:
            continue
    print("Can not find any working connection to database with pkghistory tables.")
    print("See DBSETUP.TXT file for example db setup procedure")
    sys.exit(1)

def prepare_statements(c) -> None:
    """Vytvori server-side prepared statementy"""
    c.query('PREPARE S1 (varchar) AS SELECT * FROM packages WHERE pkgdir=$1')
    c.query('PREPARE S2 (integer,varchar) AS SELECT * FROM versions WHERE packageid=$1 AND version=$2')
    c.query('PREPARE U1 (varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar) AS UPDATE packages SET section=$1,descr=$2,package=$3,maintainer=$4,rdepends=$5,bdepends=$6,www=$7,edepends=$9,pdepends=$10,fdepends=$11 WHERE pkgdir=$8')
    c.query("PREPARE I1 (varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar) AS INSERT INTO packages(package,pkgdir,section,descr,maintainer,bdepends,rdepends,www,edepends,pdepends,fdepends) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)")
    c.query('PREPARE I2 (date,date,integer,varchar) AS INSERT INTO versions(released,installed,packageid,version) VALUES($1,$2,$3,$4)')
    c.query('PREPARE Q1 (integer) AS SELECT * FROM versions WHERE packageid=$1 ORDER BY released DESC')
    c.query('PREPARE Q2 (integer,varchar) AS SELECT * FROM versions WHERE packageid=$1 AND version=$2 ORDER BY released DESC')

def loadindex(userealdates:bool=True,indexes:List[str]=[],porthints:Dict[str, Set[portdescription]]=None) -> None:
    """The -real- main() function"""
    global USEDATE
    c=findworkingdbconnection()
    prepare_statements(c)
    anyfile=False
    for fname in indexes:
        if fname[-3:]=='.gz':
            f=gzip.open(fname,'r',8192)
        else:
            f=open(fname,'r',8192)
        USEDATE=datetime.fromtimestamp(os.stat(fname)[ST_MTIME])
        anyfile=True
        loadfile(f,c,userealdates)
        f.close()
    if anyfile is False:
        if userealdates and porthints is None:
            porthints=freebsdports.loadState()
        # load default INDEX
        from .indexer import INDEX
        with open(INDEX,'r',8192) as f:
            USEDATE=datetime.fromtimestamp(os.stat(INDEX)[ST_MTIME])
            loadfile(f,c,userealdates,porthints)
    c.close()


def main() -> None:
    userealdates=True
    options, args = getopt.getopt(sys.argv[1:],"nh?")
    for opt in options:
        if opt[0]=='-n':
            userealdates=False
        elif opt[0]=='-h' or opt[0]=='-?':
            print("python",sys.argv[0]," [ options ]")
            print("This program loads /usr/ports/INDEX into PgSQL database.")
            print("Options:")
            print("  -n       Use current date not Makefile date as released date")
            print("               Program runs much faster with it")
            sys.exit(0)
    if userealdates:
        allports=freebsdports.loadState()
    else:
        allports=None
    loadindex(userealdates,args,allports)


if __name__ == '__main__':
    main()
