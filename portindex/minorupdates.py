#! /usr/bin/env python3.7
import sys
from . import bsdpkg
from . import freebsdports
from .freebsdports import portdescription
from typing import Dict, Set, List, Tuple, Optional

parsedpackage = Optional[Tuple[str, str, Optional[int], Optional[int]]]
PORTUPGRADE="portupgrade -kv "


def readportversions(ports:Dict[str, Set[portdescription]]=None) -> List[Tuple[parsedpackage, parsedpackage]]:
    """vraci seznam parsovanych dvojic (verze nase,verze v portech)"""
    if not ports:
        ports=freebsdports.loadState()
    ourversions=bsdpkg.loadoriginsfromfs()
    versions=[]
    for pkg in ourversions.keys():
        if pkg not in ports or not ports[pkg]:
            continue  # no version in ports
        nase=bsdpkg.parsepkgname(ourversions[pkg][0])
        vportech=bsdpkg.parsepkgname(next(iter(ports[pkg])).name)
        versions.append((nase,vportech))
    return versions

def twocoldisp(disp:List[str]) -> None:
    """Zobrazi retezce serazene a zformatovane do dvou sloupcu."""
    disp.sort()
    p=None
    for entry in disp:
        if p:
            print(p+entry)
            p = None
        else:
            sz=len(entry)
            entry+=(78//2-sz)*' '
            p = entry
    if p:
        print(p)

def listminorupdates(versions:List[Tuple[parsedpackage, parsedpackage]]) -> List[str]:
    """Vytiskne minor updates na obrazovku a vrati seznam baliku ktere je treba aktualizovat."""
    updates=[]
    disp=[]
    if versions is None:
        return updates
    for v in versions:
        n1,v1,r1,e1 = v[0]
        n2,v2,r2,e2 = v[1]
        # Prevedeme cisla revizi na integer abychom je mohli porovnat
        r1 = r1 or '0'
        r2 = r2 or '0'
        r1 = int(r1)
        r2 = int(r2)
        if v1 == v2 and e1 == e2 and r1 < r2:
            disp.append(n1+'-'+v1+'_'+str(r1)+" -> "+str(r2))
            updates.append(n1)
    twocoldisp(disp)
    return updates


def main() -> None:
    if len(sys.argv)>1:
        print(sys.argv[0],"takes currently no arguments")
        sys.exit(1)
    upd=listminorupdates(readportversions())
    if len(upd)>0:
        print("Found",len(upd),"packages with minor FreeBSD port version update.")


if __name__ == '__main__':
    main()
