#! /usr/bin/env python3.7
import sys
from . import freebsdports
from . import indexer
from . import loadindex
from . import updinst

def main():
    if len(sys.argv)>1:
        print(sys.argv[0],"takes currently no arguments")
        sys.exit(1)
    allports=freebsdports.loadState()
    liveports={}
    freebsdports.updatePortsInfo(allports,freebsdports.PORTS,liveports)
    freebsdports.saveState(allports)
    freebsdports.expandAllDepends(allports)
    print("Building",indexer.INDEX)
    with open(indexer.INDEX,"w") as f:
        indexer.buildINDEX(allports,f)
# Skip building portupgrade database, format is most likely out of date
# anyway.
#    print "Building",freebsdports.PORTS+'/INDEX.db'
#    portindexdb.buildnative()
# Skip building README.html files.
#    print "Updating README.html files"
#    updatereadmes.loadTemplates()
#    updatereadmes.updateCategories(allports)
#    for p in liveports.keys():
#        if liveports[p] != None and \
#           p in allports and \
#           isinstance(allports[p],freebsdports.portdescription):
#               print "Generating",updatereadmes.README,"in",p
#               updatereadmes.generateReadme(p,allports)
#    os.path.walk(freebsdports.PORTS,updatereadmes.checkForOutdatedReadme,allports)
    print("Loading INDEX into PostgreSQL database")
    loadindex.loadindex(True,[],allports)
    updinst.main(allports,True)
    print("Everything related to ports tree was updated!")


if __name__ == '__main__':
    main()
