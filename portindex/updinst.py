#! /usr/bin/env python3.7
# Updates installation status information in database
# 1) packages.install status field
# 2) packages -> version.installed date field
# 3) packages -> version.released date field to past
# 4) packages.live status field
# 5) tarballs -> pack port tarball

from .loadindex import findworkingdbconnection,prepare_statements
from .loadindex import db_loadpkg, portdescription
from datetime import datetime
from dateutil import parser
import os
from . import bsdpkg
import sys
import getopt
from . import freebsdports
import tempfile
import tarfile
import pg
from typing import BinaryIO, List, Callable, Dict, Tuple, cast, Set, Any

def getinstalledfromdb(c) -> List[str]:
    """Vrati seznam pkgdir instalovanych baliku ziskanych z db."""
    q=c.query('SELECT pkgdir FROM packages WHERE install IS TRUE')
    rc=[]
    for p in q.getresult():
        rc.append(p[0])
    return rc

def getlivefromdb(c) -> List[str]:
    """Vrati seznam instalovatelnych pkg ziskanych z db."""
    q=c.query('SELECT pkgdir FROM packages WHERE alive IS TRUE')
    rc=[]
    for p in q.getresult():
        rc.append(p[0])
    return rc

def updateinstalledstatus(c,dblistfunct:Callable[[Any], List[str]],prepquery:str,fslist:Set[str]) -> None:
    """Sesynchronizuje seznam vraceny fci dblistfunc oproti fslist"""
    c.query('BEGIN TRANSACTION')
    new=0
    old=0
    local=0
    dblist=dblistfunct(c)
    # step 1 - add missing
    for pkg in fslist:
        if pkg not in dblist:
            c.query("EXECUTE %s (TRUE,'%s')" % (prepquery,pkg))
            dblist.append(pkg)
            new+=1
    # step 2 - remove old
    for pkg in dblist:
        if pkg not in fslist:
            c.query("EXECUTE %s (FALSE,'%s')" % (prepquery,pkg))
            old+=1
    local=len(dblist)-len(dblistfunct(c))-old
    new-=local
    print(" Packages:",new,"new",old,"removed",local,"local.")
    c.query('END')

def updateinstdate(c,fslist:Dict[str,Tuple[str,str]]) -> None:
    """Aktualizuje installed status, pripadne zalozi novou verzi."""
    c.query('BEGIN TRANSACTION')
    for p in fslist.keys():
        vrs=bsdpkg.makeversion(bsdpkg.parsepkgname(fslist[p][0]))
        entry=db_loadpkg(c,p,vrs)
        if not entry:
            print(" ! Can't find version",vrs,"of package",p)
            entry=db_loadpkg(c,p)
            if entry is None or len(entry)==0:
                continue
            entry=entry[-1]
            entry.version=vrs
            entry.released=datetime.isoformat(datetime.now())
            print("new version created")
        entry.installed=fslist[p][1]
        entry.savetodb(c,False)
    c.query('END')

def tarport(portdir:str,tmpfile:BinaryIO) -> None:
    """Zapakuje tar.gz formou adresar portdir do souboru tmpfile."""
    portname=portdir[portdir.rindex('/')+1:]+'/'
    tar=tarfile.open('','w:gz',tmpfile)
    files=os.listdir(portdir)
    # remove junk entries
    try:
        files.remove('work')
    except ValueError:
        pass
    try:
        files.remove('README.html')
    except ValueError:
        pass
    # add them to the archive
    for f in files:
        tar.add(portdir+'/'+f,portname+f)
    tar.close()

def escapefile(c,f:BinaryIO) -> str:
    """Zakoduje binarni soubor do postgresql bytea encoding."""
    f.seek(0)
    data = f.read()
    return c.escape_bytea(data)

def savetar(c,tf:BinaryIO,p:str,vrs:str,stamp:str) -> None:
    """Ulozi tar.gz ze souboru tf baliku p verze vrs s casovou znackou stamp do databaze"""
    q=c.query("EXECUTE lasttardate ('%s','%s')" % (p,vrs))
    if q.getresult()[0][0] is None:
        insert=True
    else:
        insert=False
    if insert:
        c.query_prepared('inserttar',(p,vrs,escapefile(c,tf),stamp))
    else:
        c.query_prepared('updatetar',(p,vrs,escapefile(c,tf),stamp))

def updatetarballs(c,fslist: Dict[str,Tuple[str,str]],hints:Dict[str,Set[portdescription]]) -> None:
    """Zaktualizuje tarbally pro baliky s originem fslist v databazi."""
    c.query('BEGIN')
    installed=getinstalledfromdb(c)
    # abychom zabalili i master porty pridame je jak do seznamu
    # lokalne nainstalovanych, tak i do seznamu z db
    addtofslist={}
    for p in fslist.keys():
        if p not in hints:
            continue
        for m in next(iter(hints[p])).masters:
            if m not in hints:
                continue
            if m not in fslist:
                addtofslist[m]=(next(iter(hints[m])).name,fslist[p][1])
            if m not in installed:
                installed.append(m)
    fslist.update(addtofslist)
    for p in fslist.keys():
        # je balik instalovan?
        if p not in installed:
            print("Skipping",p,"because it is not installed from ports.")
            continue
        # nacist package z databaze
        ourversion=bsdpkg.parsepkgname(fslist[p][0])
        vrs=bsdpkg.makeversion(ourversion)
        entry=db_loadpkg(c,p,vrs)
        if not entry:
            # Local package
            print("No information about",p,vrs,"in database")
            continue
        # mame nainstalovanou stejnou verzi a epochu jako je v portech?
        try:
            q=next(iter(hints[p])).name
            if q:
                portversion=bsdpkg.parsepkgname(q)
            else:
                raise KeyError
        except KeyError:
            # dead package
            continue
        if ourversion[1]!=portversion[1] or \
           ourversion[3]!=portversion[3]:
            continue
        # divej se na verzi v portech, ne na lokalne nainstalovanou
        vrs=bsdpkg.makeversion(portversion)
        # zjistit zda potrebujeme updatnout ulozeny tarball
        q=c.query("EXECUTE lasttardate ('%s','%s')" % (p,vrs))
        res=q.getresult()
        #print( res[0][0],type(res[0][0]),"/",datetime.isoformat(datetime.fromtimestamp(next(iter(hints[p])).lastmod)))
        if res[0][0] is None or parser.parse(res[0][0]) < datetime.fromtimestamp(next(iter(hints[p])).lastmod):
            print("taring",p,vrs)
            # Ano. Zataruj port adresar
            tf=cast(BinaryIO,tempfile.TemporaryFile("w+b"))
            tarport(p,tf)
            # A vloz ho do databaze
            savetar(c,tf,p,vrs,datetime.isoformat(datetime.fromtimestamp(next(iter(hints[p])).lastmod)))
            tf.close()
    c.query('END')

def main(portshint:Dict[str,Set[portdescription]]=None,updatealive:bool=False) -> None:
    """Hlavni vstupni bod. Aktualizuje status nainstalovanych baliku, zabali tarbally a pripadne aktualizuje i alive status."""
    c=findworkingdbconnection()
    prepare_statements(c)
    c.query("PREPARE updinst(boolean,varchar) AS UPDATE packages SET install=$1 WHERE pkgdir=$2")
    c.query("PREPARE updlive(boolean,varchar) AS UPDATE packages SET alive=$1 WHERE pkgdir=$2")
    c.query("PREPARE lasttardate(varchar,varchar) AS SELECT MAX(packed) FROM packages,tarballs WHERE pkgdir=$1 AND tarballs.packageid=packages.id AND tarballs.version=$2")
    c.prepare("inserttar","INSERT INTO tarballs (packageid,version,tar,packed) SELECT id,$2,$3,$4 FROM packages WHERE pkgdir=$1")
    c.prepare("updatetar","UPDATE tarballs SET packed=$4, tar=$3 FROM packages WHERE packages.pkgdir=$1 AND tarballs.packageid=packages.id AND tarballs.version=$2")
    print("Loading installed packages from /var/db/pkg")
    ifs=bsdpkg.loadoriginsfromfs()
    print("",len(ifs),"installed packages found.")
    print("Updating package's installed flag in database...")
    # idb=getinstalledfromdb(c)
    updateinstalledstatus(c,getinstalledfromdb,'updinst',set(ifs.keys()))
    print("Updating installed dates in database...")
    updateinstdate(c,ifs)
    if portshint:
        newhints={}
        # remove includes from portshints
        for p in portshint.keys():
            # top level value must be dict
            if isinstance(portshint[p],set):
                # not empty
                if len(portshint[p]) > 0:
                    # and must have portdescription object inside
                    if isinstance(next(iter(portshint[p])), freebsdports.portdescription):
                        newhints[p]=portshint[p]
        portshint = newhints
    if portshint:
        print("Updating portbld tarballs in database...")
        updatetarballs(c,ifs,portshint)
    if portshint is not None and updatealive:
        print(len(portshint),"total packages currently available from ports.")
        print("Updating package's live flag in database...")
        updateinstalledstatus(c,getlivefromdb,'updlive',portshint.keys())
    c.close()


def entry() -> None:
    updatealive=False
    for opt in getopt.getopt(sys.argv[1:],"ah?")[0]:
        if opt[0]=='-a':
            updatealive=True
        elif opt[0]=='-h' or opt[0]=='-?':
            print("python",sys.argv[0]," [ options ]")
            print("This program updates installed/live flag in package database")
            print("Options:")
            print("  -a       update live flag from portindex state info. (default off).")
            print("  -h       This help")
            sys.exit(0)

    hints=freebsdports.loadState()
    if len(hints)==0:
        hints=None
    main(hints,updatealive)


if __name__ == "__main__":
    entry()
