#! /usr/bin/env python3.7
import sys
import getopt
from . import minorupdates
from .minorupdates import parsedpackage
from typing import List, Tuple


def listpointupdates(versions:List[Tuple[parsedpackage, parsedpackage]],mode:int=1) -> List[str]:
    """mode=1 point, mode=2 major, mode=3 both Print updates on the screen and return package names"""
    updates=[]
    disp=[]
    if versions is None:
        return []
    for v in versions:
        n1,v1,r1,e1 = v[0]
        n2,v2,r2,e2 = v[1]
        if v1 != v2 and e1 == e2:
            nase=v1.split('.')
            porty=v2.split('.')
            if len(nase)==len(porty):
                porovnej=len(nase)-1
            else:
                porovnej=min(len(nase), len(porty))
            porovnej=min(max(1,porovnej),2)
            shodne=True
            for i in range(0,porovnej):
                if nase[i]!=porty[i]:
                    shodne=False
                    break
            if (mode == 1 and shodne) or \
               (mode == 2 and not shodne) or \
               (mode > 2):
                disp.append(n1+'-'+v1+" -> "+v2)
                updates.append(n1)
    minorupdates.twocoldisp(disp)
    return updates


def main() -> None:
    mode = 1
    for opt in getopt.getopt(sys.argv[1:],"mpah?")[0]:
        if opt[0]=='-a':
            mode=3
        elif opt[0]=='-m':
            mode=2
        elif opt[0]=='-p':
            mode=1
        elif opt[0]=='-h' or opt[0]=='-?':
            print("python",sys.argv[0]," [ options ]")
            print("This program displays updated ports.")
            print("Options:")
            print("  -m       display only major upstream version updates.")
            print("  -p       display only point upstream version updates.")
            print("  -a       display all upstream version updates.")
            print("  -h       This help")
            sys.exit(0)
    upd=listpointupdates(minorupdates.readportversions(),mode)
    if len(upd)>0:
        print("Found",len(upd),"packages with", end='')
        if mode == 1:
            print("point",end='')
        elif mode == 2:
            print("major",end='')
        print("version update.")


if __name__ == '__main__':
    main()
