#! /usr/bin/env python3.7
# Move ports by reading /usr/ports/MOVED
import sys
from . import loadindex
import getopt
from . import freebsdports
import pg
from typing import TextIO


def loadmoved(c,f:TextIO,ask:bool=False):
    """Nahraje a zpracuje /usr/ports/MOVED soubor"""
    q=c.query('PREPARE getversions(integer) AS SELECT version,released FROM versions WHERE versions.packageid=$1 ORDER BY released DESC')
    for line in f.readlines():
        if line.startswith("#"):
            continue
        parts=line.split("|")
        if len(parts) != 4:
            continue
        if len(parts[1]) == 0:
            continue
        # najdeme zda existuji oba zminene baliky
        q=c.query("EXECUTE S1('%s')" % (freebsdports.PORTS+'/'+parts[0]))
        if q.ntuples() == 0:
            continue
        res=q.dictresult()
        if res[0]['alive'] or res[0]['install']:
            continue
        q=c.query("EXECUTE S1('%s')" % (freebsdports.PORTS+'/'+parts[1]))
        res2=q.dictresult()
        if q.ntuples() == 0:
            continue
        print("Date",parts[2],"package",parts[0],'moved to',parts[1])
        print("  Reason:",parts[3],end='')
        print("  Available versions:")
        print("\t",parts[0],"\t",parts[1])
        q=c.query("EXECUTE getversions(%d)" % res[0]['id'])
        ver=q.dictresult()
        q=c.query("EXECUTE getversions(%d)" % res2[0]['id'])
        ver2=q.dictresult()
        numspaces=len(parts[0])
        for i in range(max(len(ver),len(ver2))):
            try:
                print("\t",ver[i]['version'].ljust(numspaces),end='')
            except IndexError:
                print("".ljust(numspaces),end='')
            try:
                print("\t",ver2[i]['version'])
            except IndexError:
                print()
        print("What to do: d (delete old), m (merge old), i (ignore)? [i]",end='')
        reply=input()
        if reply=='d':
            c.query("BEGIN")
            c.query("DELETE FROM tarballs WHERE packageid="+str(res[0]['id']))
            c.query("DELETE FROM versions WHERE packageid="+str(res[0]['id']))
            c.query("DELETE FROM packages WHERE id="+str(res[0]['id']))
            c.query("COMMIT")
            print("Deleted.")
            continue
        if reply=='q':
            print("Quit.")
            c.close()
            sys.exit(0)
        if reply=='m':
            c.query("BEGIN")
            # Delete clashing versions from the old port
            c.query("DELETE FROM tarballs WHERE packageid="+str(res[0]['id'])+" AND version IN (SELECT version FROM tarballs WHERE packageid="+str(res2[0]['id'])+")")
            c.query("DELETE FROM versions WHERE packageid="+str(res[0]['id'])+" AND version IN (SELECT version FROM versions WHERE packageid="+str(res2[0]['id'])+")")
            # Merge them
            c.query("UPDATE tarballs SET packageid="+str(res2[0]['id'])+" WHERE packageid="+str(res[0]['id']))
            c.query("UPDATE versions SET packageid="+str(res2[0]['id'])+" WHERE packageid="+str(res[0]['id']))
            # Drop old main record
            c.query("DELETE from packages WHERE id="+str(res[0]['id']))
            c.query("COMMIT")
            print("Merged.")
            continue

        print("Ignored.")


def main() -> None:
    ask=True
    options, args = getopt.getopt(sys.argv[1:],"yh?")
    for opt in options:
        if opt[0]=='-y':
            ask=False
        elif opt[0]=='-h' or opt[0]=='-?':
            print("python",sys.argv[0]," [ options ]")
            print("This program applies changes from /usr/ports/MOVED")
            print("Options:")
            print("  -y       Do not ask for confirmation")
            sys.exit(0)
    c=loadindex.findworkingdbconnection()
    loadindex.prepare_statements(c)
    f=open(freebsdports.PORTS+'/MOVED','r')
    loadmoved(c,f,ask)
    f.close()
    c.close()


if __name__ == '__main__':
    main()
