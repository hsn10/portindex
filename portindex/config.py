# Configuration file support for package
import os

CONFIGFILE='portindex.conf'
SYSINCIGNORETIME=0
DATABASE_CONNECTIONS = ['','root','pkghistory','ports']
KEEP_DUPES=True
KEEP_UNRESOLVED=False
SERIAL=0

# try to process configuration
try:
    if os.path.isfile(CONFIGFILE) and os.access(CONFIGFILE,os.R_OK):
        exec(open(CONFIGFILE).read(),globals())
except Exception:
    print("!! ERROR while processing",CONFIGFILE,"config file. Check syntax!")
