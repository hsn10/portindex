#! /usr/bin/env python3.7

import os
import sys
from . import freebsdports
from . import config
import re
from typing import Dict, Set, TextIO

def buildINDEX(ports:Dict[str,Set[freebsdports.portdescription]],output:TextIO) -> None:
    dupelist={}    # For ports duplicated by name
    dupes=0
    unresolved=0
    toberemoved=set()
    for s in ports.values():
        for p in s:
            if p.name is None:
                if p.path[0] == '/':
                    print("ERROR: Removing port without name in",p.path)
                    toberemoved.add(p.path)
                continue
            if p.name in dupelist:
                print("Duplicate port",p.name,"in both",p.path,"and",dupelist[p.name])
                if config.KEEP_DUPES is False:
                    continue
                dupes+=1
            dupelist[p.name]=p.path
            try:
                for m in p.masters:
                    freebsdports.directoriesToNames(m,ports,config.KEEP_UNRESOLVED)
                output.write("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n" % (p.name,p.path,p.prefix,index_escape(p.comment),p.dfile,index_escape(p.maintainer),p.categories,freebsdports.directoriesToNames(p.bdeps,ports,config.KEEP_UNRESOLVED),freebsdports.directoriesToNames(p.rdeps,ports,config.KEEP_UNRESOLVED),p.www,freebsdports.directoriesToNames(p.edeps,ports,config.KEEP_UNRESOLVED),freebsdports.directoriesToNames(p.pdeps,ports,config.KEEP_UNRESOLVED),freebsdports.directoriesToNames(p.fdeps,ports,config.KEEP_UNRESOLVED)))
            except KeyError:
                unresolved+=1
                _junk, value = sys.exc_info()[:2]
                print("Removing port",p.name,"due to unresolved dependences",value)
                toberemoved.add(p.path)
    print(len(dupelist)+dupes-unresolved,'total index entries.')
    if dupes>0:
        print(dupes,"duplicated entries kept.")
    if unresolved>0:
        print(unresolved,"ports with unresolved dependences discarded.")
    for deadpath in toberemoved:
        try:
            del ports[deadpath]
        except KeyError:
            pass

def index_escape(what:str) -> str:
    """Escape special characters for INDEX output"""
    return re.sub(r"(['\*])",r"\\\1",what)


INDEX=freebsdports.PORTS+"/INDEX"
if sys.platform != "win32":
    INDEX+="-"+re.match(r'^\d+', os.uname()[2]).group(0)

def main() -> None:
    if len(sys.argv)==1:
        output = INDEX
    elif len(sys.argv)==2:
        output = sys.argv[1]
    else:
        print(sys.argv[0]," [INDEXFILE]")
        sys.exit(1)
    allports=freebsdports.loadState()
    freebsdports.updatePortsInfo(allports)
    freebsdports.saveState(allports)
    freebsdports.expandAllDepends(allports)
    print("Building",output)
    with open(output,"w") as f:
        buildINDEX(allports,f)


if __name__=="__main__":
    main()
