#! /usr/bin/env python3.7
import sys

from .loadindex import findworkingdbconnection, pkg, prepare_statements
from .getport import gettarball
from .query import find_package
import difflib
import tarfile
import tempfile
from typing import List, Optional

def findmakefile(tarmembers:List[tarfile.TarInfo]) -> Optional[tarfile.TarInfo]:
    for ti in tarmembers:
        if ti.name.endswith("Makefile"):
            return ti
    return None

def showdelta(tar1:bytes,tar2:bytes) -> None:
    # write tarballs to temporary files
    tf1=tempfile.TemporaryFile()
    tf1.write(tar1)
    tf1.seek(0)
    tf2=tempfile.TemporaryFile()
    tf2.write(tar2)
    tf2.seek(0)
    # open tars
    ball1=tarfile.open("","r",tf1)
    ball2=tarfile.open("","r",tf2)
    # find Makefile inside
    make1=ball1.extractfile(findmakefile(ball1.getmembers()))
    make2=ball2.extractfile(findmakefile(ball2.getmembers()))
    delta=difflib.unified_diff(list(map(lambda x: x.decode(),make1.readlines())),list(map(lambda x: x.decode(),make2.readlines())))
    for line in delta:
        sys.stdout.write(line)


def main() -> None:
    if len(sys.argv)!=4:
        print("python",sys.argv[0],"<package> <version1> <version2>")
    else:
        c=findworkingdbconnection()
        prepare_statements(c)
        package,ver1,ver2=sys.argv[1:]
        pkg_found=find_package(c,package)
        if pkg_found is not None:
            pkg1 = pkg()
            pkg1.updatefromdb(pkg_found, pkg_found)
            tar1=gettarball(c,pkg1,ver1)
            if tar1 is None:
                print("Can not find tarball from version",ver1,"of",package)
            else:
                tar2=gettarball(c,pkg1,ver2)
                if tar2 is None:
                    print("Can not find tarball from version",ver2,"of",package)
                else:
                    tar1=tar1[0]
                    tar2=tar2[0]
                    showdelta(tar1,tar2)
        c.close()


if __name__ == '__main__':
    main()
