#! /usr/bin/env python3.7
import sys
import getopt
import os
from typing import Dict, Any, Optional

from .loadindex import findworkingdbconnection
from .freebsdports import PORTLEN,PORTS


def find_package(c,pkg:str) -> Optional[Dict[str,Any]]:
    """Najde co mozna nejpresneji balik v databazi. Vraci dictresult zaznamu z packages tabulky nebo None"""
    query='SELECT * FROM packages WHERE '
    if '/' in pkg:
        query+='pkgdir=\''+PORTS+'/'
    else:
        query+='package=\''
    query+=pkg+'\''
    q=c.query(query)
    if q.ntuples()==0:
        # try to do exact pkgdir search
        q=c.query("SELECT * FROM packages WHERE pkgdir LIKE '%/"+pkg+"'")
    if q.ntuples()==0:
        # try pkgdir *
        q=c.query("SELECT * FROM packages WHERE pkgdir LIKE '%/"+pkg+"%'")
    if q.ntuples()==0:
        # try package *
        q=c.query("SELECT * FROM packages WHERE package LIKE '"+pkg+"%'")
    if q.ntuples()==0:
        q=c.query("SELECT * FROM packages WHERE package LIKE '%"+pkg+"%' OR pkgdir LIKE '%"+pkg+"%'")
    if q.ntuples()!=1:
        if q.ntuples()==0:
            print("Package",pkg,"not found in database.")
            return None
        else:
            print("Package",pkg,"found multiple times in database.")
            print("Please choose one of",pkg,"packages available")
            for result in q.dictresult():
                print(" ",result['pkgdir'][PORTLEN+1:],result['descr'])
            return None
    return q.dictresult()[0]

def show_info(c,pkg:str) -> None:
    res=find_package(c,pkg)
    if res is None:
        return
    q=c.query('SELECT *,versions.version FROM packages,versions WHERE packages.id=%d AND versions.packageid=%d AND versions.released=(SELECT max(released) FROM versions WHERE versions.packageid=%d)' % (3*(res['id'],)))
    res=q.dictresult()[0]
    print('Package: %s %s in %s\nDescription: %s\nMaintainer: %s\nWWW: %s\nDepends on: %s' % (res['package'],res['version'],res['pkgdir'],res['descr'],res['maintainer'],res['www'],res['rdepends']))
    # display distinfo
    if os.access(res['pkgdir']+'/distinfo',os.R_OK):
        print(open(res['pkgdir']+'/distinfo','r').read())

def show_versions(c,pkg:str) -> bool:
    """Vrati true pokud balik existuje"""
    res=find_package(c,pkg)
    if res is None:
        return False
    # use better name if possible
    if len(res['package'])> len(pkg):
        pkg=res['package']

    if not res['alive']:
        print("Package",pkg,"is no longer available in ports.")
    inst=0
    if not res['install']:
        print("Package",pkg,"is not installed.")
        inst=0
    else:
        inst=1
        q=c.query("SELECT 'now'-MAX(installed) FROM versions WHERE packageid="+str(res['id']))
        res2=q.getresult()
        if q.ntuples():
            print("Package",pkg,"was installed",res2[0][0],"days ago.")
        else:
            print("Package",pkg,"is installed.")
            inst=0

    # Report available versions
    q=c.query('SELECT version,released,installed,exists(SELECT 1 FROM tarballs WHERE tarballs.version=versions.version AND tarballs.packageid=versions.packageid) AS havetar FROM versions WHERE versions.packageid='+str(res['id'])+' ORDER BY released DESC')
    res2=q.dictresult()
    for result in res2:
        print("pkg",pkg,result['version'],result['released'],end='')
        if result['installed']:
            print(" ( installed",result['installed'],')',end='')
        if result['havetar']:
            print('*',end='')
        print()
    q=c.query("SELECT 'now'-MAX(released) FROM versions WHERE packageid="+str(res['id']))
    res2=q.getresult()
    print("Latest available version of",pkg,"is",res2[0][0],"days old.")
    if inst:
        q=c.query("SELECT 'now'- CASE released>installed WHEN true THEN installed ELSE released END FROM versions WHERE packageid="+str(res['id'])+" AND installed=(SELECT MAX(installed) FROM versions WHERE packageid="+str(res['id'])+");")
        res2=q.getresult()
        print("Installed version is",res2[0][0],"days old.")
    return bool(inst)


def main() -> None:
    options=getopt.getopt(sys.argv[1:],'ih?')
    info=False
    for opt in options[0]:
        if opt[0]=='-i':
            info=True
        else:
            print("python",sys.argv[0]," [ options ] package ...")
            print("This program displays history of given package.")
            print("Options:")
            print("  -i       display additional information.")
            print("  -h, -?   This help")
            sys.exit(0)

    c=findworkingdbconnection()
    for pkgname in options[1]:
        if info:
            show_info(c,pkgname)
        else:
            show_versions(c,pkgname)
    c.close()


if __name__ == '__main__':
    main()