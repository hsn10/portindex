NAME=bsdportsutils
VERSION=33
LVERSION:=	`expr $(VERSION) - 4`
ARCHIVE=	$(NAME)-$(VERSION).tar.gz

default::

changelog::
	@rm -f CHANGELOG.TXT
	@echo "Generating CHANGELOG.TXT"
	git log  --abbrev-commit --format=medium refs/tags/$(LVERSION)..HEAD >> CHANGELOG.TXT
package:: clean changelog
	tar czvof $(ARCHIVE) portindex/*.py tests/*.py Makefile *.TXT *.sql *.conf setup.py
	-advdef -4z $(ARCHIVE)
	rm -f CHANGELOG.TXT
tar::	package
zip::	package
dist::	package
install:: package
	cp $(ARCHIVE) /usr/ports/distfiles
	mv $(ARCHIVE) $(HOME)/public_html/distfiles
clean::
	rm -f $(NAME)-*.tar.gz portindex/*.pyc portindex/*.pyo tests/*.pyc tests/*.pyo CHANGELOG.TXT
	rm -rf portindex/__pycache__ tests/__pycache__
