from setuptools import setup,find_packages

setup(
    name = "portindex",
    version = "33",
    author = "Radim Kolar",
    author_email = "hsn@sendmail.cz",
    license = "AGPL3",
    url = "https://gitlab.com/hsn10/portindex",
    description = "Tools for managing and tracking FreeBSD ports",
    packages=find_packages(exclude=["tests"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 6 - Mature",
        "Environment :: Console",
        "Intended Audience :: Information Technology",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: POSIX :: BSD :: FreeBSD",
        "Topic :: System :: Installation/Setup"
    ],
    install_requires=["attrs>=18"],
    extras_require= {
        "HISTORY": ["PyGreSQL", "python-dateutil"]
    },
    python_requires='>=3.7',
    entry_points={
        "console_scripts": [
            "getport = portindex.getport:main [HISTORY]",
            "loadindex = portindex.loadindex:main [HISTORY]",
            "loadmoved = portindex.loadmoved:main [HISTORY]",
            "minorupdates = portindex.minorupdates:main",
            "pointupdates = portindex.pointupdates:main",
            "portindex = portindex.indexer:main",
            "portreadmes = portindex.updatereadmes:main",
            "query = portindex.query:main [HISTORY]",
            "querydelta = portindex.querydelta:main [HISTORY]",
            "stealthupdates = portindex.stealthupdates:main",
            "updateall = portindex.updateall:main [HISTORY]",
            "updinst = portindex.updinst:entry [HISTORY]"
        ]
    }
)
