from portindex.loadindex import pkg
from portindex.indexer import index_escape


def test_unescape():
    assert pkg.unescape_index(r"\*") == "*"
    assert pkg.unescape_index(r"\'") == "'"
    assert pkg.unescape_index(r"\*\*") == "**"
    assert pkg.unescape_index("") == ""
    assert pkg.unescape_index(None) is None

def test_escape():
    assert index_escape("*") == r"\*"
    assert index_escape("'") == r"\'"
    assert index_escape("**") == r"\*\*"
    assert index_escape("") == ""
