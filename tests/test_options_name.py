from portindex.freebsdports import portdescription


def test_options_file_name():
    """Jmeno options souboru ma priponu /var/db/ports pak jmeno kategorie
    podtrzitko a jmeno adresare v kterem je port.
    """
    p = portdescription("/usr/ports/x11/9menu")
    p.categories="x11 plan9"
    assert p.path == "/usr/ports/x11/9menu"
    assert p.optionsfile() == "/var/db/ports/x11_9menu/options"
