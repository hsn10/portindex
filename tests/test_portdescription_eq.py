import unittest
from portindex.freebsdports import portdescription


class portdescriptionEQ(unittest.TestCase):

    NAME = "testport-1.0.3"
    PATH = "/usr/ports/misc/testport"

    def setUp(self):
        self.a = portdescription(portdescriptionEQ.PATH)
        self.a.name = portdescriptionEQ.NAME
        self.b = portdescription(portdescriptionEQ.PATH)
        self.b.name = portdescriptionEQ.NAME

    def test_Basicequals(self):
        self.assertTrue( self.a == self.b)

    def test_differentName_SamePath(self):
        self.a.name = "different"
        self.assertFalse( self.a == self. b)

    def test_sameNameDifferentPath(self):
        self.a.path = "different"
        self.assertFalse(self.a == self.b)

    def test_differentComment(self):
        self.a.comment = "1"
        self.b.comment = "2"
        self.assertTrue(self.a == self.b)


if __name__ == '__main__':
    unittest.main()
