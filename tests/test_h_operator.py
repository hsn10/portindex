from portindex.freebsdports import portdescription

def test_emptyH():
    assert portdescription.H_operator("/usr") == ""

def test_normalH():
    assert portdescription.H_operator("/usr/bin/make") == "/usr/bin"
