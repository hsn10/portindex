from portindex.freebsdports import portdescription


def preparePort():
    p = portdescription("/usr/ports/misc/mc")
    assert not p.sysinc
    return p


def test_uses_plain():
    p = preparePort()
    p.addUsesIncludes("USES=\tpython\n")
    assert "Uses/python.mk" in p.sysinc


def test_uses_with_version():
    p = preparePort()
    p.addUsesIncludes("USES=\tpython:2.7\n")
    assert "Uses/python.mk" in p.sysinc

