\echo Dropping old objects
drop table if exists versions;
drop table if exists packages;
drop table if exists tarballs;
\echo Creating new objects
create table packages (
	id int4 generated always as identity primary key,
	pkgdir  varchar(80) not null,
	package varchar(80) not null,
	section varchar(80),
	maintainer varchar(80),
	bdepends varchar,
	rdepends varchar,
	fdepends varchar,
    edepends varchar,
    pdepends varchar,
	alive boolean default TRUE not null,
	install boolean default FALSE not null,
	descr varchar(160),
	www varchar(200)
);

create table versions (
	packageid int4 references packages(id) not null,
	version varchar(80) not null,
	released date not null default now(),
	installed date default NULL
);

create table tarballs (
	packageid int4 references packages(id) not null,
	version varchar(80) not null,
    packed timestamp without time zone not null default now(),
	tar bytea not null
);

create unique index packages1 on packages(pkgdir);
create unique index versions1 on versions(packageid,version);

create index packages2 on packages(package);
