BEGIN TRANSACTION ISOLATION LEVEL SERIALIZABLE;

CREATE TEMPORARY TABLE dead (id int4) ON COMMIT DROP;
INSERT INTO dead(id) select id from packages where alive is false;
DELETE FROM tarballs WHERE packageid in (select * from dead);
DELETE FROM versions where packageid in (select * from dead);
DELETE FROM packages where id in (select * from dead);
COMMIT WORK;

DELETE FROM versions WHERE (packageid,version) in (select packageid,version from versions v where packageid in (SELECT packageid from versions group by packageid having count(*)>15) and (packageid,version) not in (select packageid,version from versions where packageid=v.packageid order by released desc limit 15));

DELETE FROM tarballs WHERE NOT EXISTS (select 1 from versions WHERE
 tarballs.version=versions.version AND tarballs.packageid = versions.packageid);
